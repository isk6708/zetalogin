<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\Reference;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\LeaveApplication;


class LeaveController extends Controller
{
    function SenaraiCuti(Request $req){
        if (isset($req->leave_type) && !empty($req->leave_type))
            $leaves = Leave::where('leave_type',$req->leave_type)->paginate(3);
        else
            $leaves = Leave::paginate(3);  

        $leave_type = $req->leave_type;
        $leave_types = Reference::where('cat','LEAVE')->get();

        return view('leave.senarai_cuti',compact('leaves','leave_type','leave_types'));
    }

    function MohonCuti($id=''){
        $pemohon = Auth::user()->name;
        if($id == '')
            $leave = new Leave();
        else
            $leave = Leave::find($id);

        $leave_types = Reference::where('cat','LEAVE')->get();
        $status_codes = Reference::where('cat','LVSTS')->get();

        return view('leave.mohon_cuti',compact('pemohon','leave','leave_types','status_codes'));
    }

    function SimpanCuti(Request $req,$id=''){

        

        $messages = [
            'leave_type.required' => 'Jenis Cuti adalah wajib',
            'start_dt.required' => 'Tarikh Mula adalah wajib',
            'end_dt.required' => 'Tarikh Akhir adalah wajib',
            'status_code.required' => 'Status Cuti adalah wajib',
            'attachment.required' => 'Lampiran adalah wajib',
        ];

        $validated = $req->validate([
            'leave_type' => 'required',
            'start_dt' => 'required',
            'end_dt' => 'required',
            'status_code' => 'required',
            'attachment' => 'required',
            
        ], $messages);        

        if($id == '')
            $leave = new Leave();
        else
            $leave = Leave::find($id);

        $leave->leave_type = $req->leave_type;
        $leave->start_dt = $req->start_dt;
        $leave->end_dt = $req->end_dt;
        $leave->status_code = $req->status_code;
        if ($leave->status_code == '10'){
            $leave->user_id = Auth::user()->id;
        }
        $leave->save();

        if ($leave->status_code == '10'){

            $email   = 'hradmin@zetasb.com';
            // $subject = 'Permohonan Cuti dari '.Auth::user()->name;
            // $content = "Permohonan Cuti dari ". Auth::user()->name." pada ". 
            // date('d-m-Y',strtotime($leave->start_dt));

            // Mail::raw($content, function ($message) use($email, $subject) {
            // $message->to($email)->subject($subject);
            // });

            Mail::to($email)->send(new LeaveApplication($leave));
        }elseif($leave->status_code == '99'){
            $email   = Auth::user()->email;
            Mail::to($email)->send(new LeaveApplication($leave));
        }

        return redirect('/senarai-cuti') ;
    }

    function BatalCuti(){
        echo 'Batal Cuti';
    }

    function StatusCuti($jeniscuti,$status='10'){
        switch($status){
            case '02':
                echo 'Pengesahan Cuti';    
                break;
            case '03':
                echo 'Kelulusan Cuti';    
                break;
            case '09':
                echo 'Batal Cuti';    
                break;
            default:
            echo 'Mohon Cuti';
        }
    
        echo '<br>';
        echo 'Jenis Cuti'.$jeniscuti;
    }

    function HapusCuti($id){
        Leave::find($id)->delete();
        return redirect('/senarai-cuti') ;
    }
}
