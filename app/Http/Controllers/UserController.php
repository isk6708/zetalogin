<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();

        // if(Storage::disk('public')->exists('avatars/'.Auth::user()->id.'.jfif'))
        // $url = Storage::url('avatars/'.Auth::user()->id.'.jfif');
        // else
        $avatar = Storage::url('avatars/default.jfif');

        return view('user._form',compact('user','avatar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        // dd($req);
        $req->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'katalaluan' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->katalaluan);
        $user->save();

        $file = $req->file('attachment');
        if (isset($file)){
            $extension = $file->getClientOriginalExtension();
            $rename = $user->id.'.'.$extension;        
            $req->file('attachment')->storeAs('public/avatars', $rename);
        }
        

        return redirect(route('pengguna.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if(Storage::disk('public')->exists('avatars/'.$user->id.'.jfif'))
        $avatar = Storage::url('avatars/'.$user->id.'.jfif');
        else
        $avatar = Storage::url('avatars/default.jfif');

        return view('user._form',compact('user','avatar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $req->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'katalaluan' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::find($id);
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->katalaluan);
        $user->save();

        $file = $req->file('attachment');
        // dd($file);
        if (isset($file)){
            // dd($file);
            $extension = $file->getClientOriginalExtension();
            $rename = $user->id.'.'.$extension;        
            $req->file('attachment')->storeAs('public/avatars', $rename);
        }
        

        return redirect(route('pengguna.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return redirect(route('pengguna.index'));
    }
}
