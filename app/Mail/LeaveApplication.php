<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Mail\Mailables\Address;
use App\Models\Leave;

class LeaveApplication extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(protected Leave $leave)
    {
        
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        if ($this->leave->status_code == '99'){
            return new Envelope(
                from: new Address(Auth::user()->email, Auth::user()->name),
                subject: 'Leave Application Rejected For '.$this->leave->pemohon->name,
            );
        }else{
            return new Envelope(
                from: new Address(Auth::user()->email, Auth::user()->name),
                subject: 'Leave Application From '.Auth::user()->name,
            );
        }
        
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'leave.email.leave_application',
            with: [
                'pic' => Auth::user()->name,
                'pemohon' => $this->leave->pemohon->name,
                'status' => $this->leave->status_code,
                'jenis_cuti' => $this->leave->refs->descr,
                'tarikh_mula' => date('d-m-Y',strtotime($this->leave->start_dt)),
                'tarikh_akhir' => date('d-m-Y',strtotime($this->leave->end_dt)),
            ],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
