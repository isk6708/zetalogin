<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    use HasFactory;
    protected $table = 'leave_app';
    public $timestamps = false;

    function refs(){
        return $this->hasOne(Reference::class,'code','leave_type')->where('cat','LEAVE');
    }

    function lvsts(){
        return $this->hasOne(Reference::class,'code','status_code')->where('cat','LVSTS');
    }

    function pemohon(){
        return $this->hasOne(User::class,'id','user_id');
    }

}

