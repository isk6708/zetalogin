<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('references')->where('cat','=','LEAVE')->delete();
        $arr = [
            ['cat' => 'LEAVE','code' => '10','descr' => 'Cuti Rehat'],
            ['cat' => 'LEAVE','code' => '11','descr' => 'Cuti Sakit'],
            ['cat' => 'LEAVE','code' => '12','descr' => 'Cuti Bersalin'],
            ['cat' => 'LEAVE','code' => '13','descr' => 'Cuti Mengerjakan Ibadah Haji'],
            ['cat' => 'LEAVE','code' => '14','descr' => 'Cuti Mengerjakan Ibadah Umrah'],
            ['cat' => 'LEAVE','code' => '15','descr' => 'Cuti Kerantina'],
        ];

        DB::table('references')->insert($arr);

        DB::table('references')->where('cat','=','CLAIM')->delete();
        $arr = [
            ['cat' => 'CLAIM','code' => '10','descr' => 'Tuntutan Perjalanan'],
            ['cat' => 'CLAIM','code' => '11','descr' => 'Hotel'],
        ];
        DB::table('references')->insert($arr);

        DB::table('references')->where('cat','=','LVSTS')->delete();
        $arr = [
            ['cat' => 'LVSTS','code' => '10','descr' => 'Permohonan Cuti'],
            ['cat' => 'LVSTS','code' => '11','descr' => 'Lulus Cuti'],
            ['cat' => 'LVSTS','code' => '98','descr' => 'Batal Cuti'],
            ['cat' => 'LVSTS','code' => '99','descr' => 'Permohonan Cuti Ditolak'],
        ];
        DB::table('references')->insert($arr);

    }
}
