<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Zeta SB</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      @auth
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/senarai-cuti">Cuti</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('pengguna.index')}}">Selenggara Pengguna</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/profile">My Profile</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Gaji & Elaun
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Slip Gaji</a></li>
            <li><a class="dropdown-item" href="#">Jadual Gaji</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Tangga Gaji</a></li>
          </ul>
        </li>
        @endauth

        @auth
            <li class="nav-item">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a class="nav-link" href="{{ route('logout') }}" 
                        onclick="event.preventDefault();
                        this.closest('form').submit();">
                        Log Keluar</a>
                </form>
            </li>
        @endauth

        @guest
            <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Log Masuk</a>
            </li>

            @if (Route::has('register'))
                <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">Daftar Pengguna</a>
                </li>
            @endif
        @endguest
        
      </ul>
    </div>
  </div>
</nav>