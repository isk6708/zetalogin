@if($status=='10')
<h1>
    Permohonan cuti dari {{ $pemohon }}
</h1>
<p>
    Salam Sejahtera Tuan Pentadbir
</p>

<p>
    Bersama ini dimajukan permohonan cuti {{ $jenis_cuti }} dari {{ $pemohon }}.
<br>
    Cuti akan bermula pada {{ $tarikh_mula }} sehingga {{ $tarikh_akhir }}
    <br>
    Semoga permohonan ini diluluskan. 
    
</p>
@elseif($status=='99')
    <h1>
    Permohona cuti dari {{ $pemohon }} di tolak
</h1>
<p>
    Salam Sejahtera {{ $pemohon }}
</p>

<p>
    Bersama ini dimaklumkan permohonan cuti {{ $jenis_cuti }} dari {{ $pemohon }}.
<br>
    bermula pada {{ $tarikh_mula }} sehingga {{ $tarikh_akhir }} di BATALKAN
    <br>
    Jangan Sediah 
    
</p>

@endif
<p>
<br><br>
    Terima kasih
    <br><br>
    @if($status=='99')
    Yang menjalankan tugas,
    <br>
    {{$pic}}
    @endif
    <br><br>
    Email ini adalah dijana oleh sistem.
</p>