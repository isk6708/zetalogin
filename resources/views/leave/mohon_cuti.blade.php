@extends('layouts.zeta_login')
@section('title', 'Borang Cuti')
@section('content')
    <h1>Mohon Cuti oleh {{$pemohon}}</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($leave->id))
    <form action="/simpan-cuti/{{$leave->id}}" method='POST'>
    @else
    <form action="/simpan-cuti" method='POST' enctype="multipart/form-data">
    @endif
        @csrf
        <div class="col-sm-4">
            <div class="mb-3">
                <label for="leave_type" class="form-label">Jenis Cuti <span class="mandatory">*</span></label>
                <select name="leave_type" id="leave_type" class="form-control">
                    <option value="">- Sila Pilih -</option>
                    @foreach($leave_types as $m)
                    <option value="{{$m->code}}" {{$m->code == $leave->leave_type? 'selected':''}}>{{$m->descr}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="start_dt" class="form-label">Tarikh Mula<span class="mandatory">*</span></label>
                <input type="date" name="start_dt" id="start_dt" value="{{old('start_dt', $leave)}}" class="form-control">
            </div>
            <div class="mb-3">
                <label for="end_dt" class="form-label">Tarikh Akhir<span class="mandatory">*</span></label>
                <input type="date" name="end_dt" id="end_dt" value="{{old('end_dt', $leave->end_dt)}}" class="form-control">
            </div>
            <div class="mb-3">
                <label for="status_code" class="form-label">Status Cuti <span class="mandatory">*</span></label>
                <select name="status_code" id="status_code" class="form-control">
                    <option value="">- Sila Pilih -</option>
                    @foreach($status_codes as $m)
                    <option value="{{$m->code}}" {{$m->code == $leave->status_code? 'selected':''}}>{{$m->descr}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="end_dt" class="form-label">Lampiran<span class="mandatory">*</span></label>
                <input type="file" name="attachment" id="attachment" class="form-control">
            </div>
            <div class="mb-3">
                <input type="submit" value="{{empty($leave->id)?'Simpan':'Kemaskini'}}" class='btn btn-primary'>
                @if(!empty($leave->id))
                <a href="/borang-cuti/{{$leave->id}}" class='btn btn-secondary'>Set Semula</a>
                @else
                <a href="/borang-cuti" class='btn btn-secondary'>Set Semula</a>
                @endif
                <a href="/senarai-cuti" class='btn btn-danger'>Kembali</a>
            </div>
        </div>

    </form>
    @endsection