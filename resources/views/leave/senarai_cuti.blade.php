@extends('layouts.zeta_login')
@section('title', 'Senarai Cuti')
@section('content')
    <h1>Senarai Cuti</h1>
    <form action="/senarai-cuti">

        <div class="mb-3">
            <label for="leave_type" class="form-label">Jenis Cuti</label>
            <select name="leave_type" id="leave_type" class="form-select">
                    <option value="">- Sila Pilih -</option>
                    @foreach($leave_types as $m)
                    <option value="{{$m->code}}" {{$m->code == $leave_type? 'selected':''}}>{{$m->descr}}</option>
                    @endforeach
                </select>
        </div>
        <div class="mb-3">
        <input type="submit" value="Teruskan" class="btn btn-success">
        <a href="/senarai-cuti" class='btn btn-secondary'>Set Semula</a>
        </div>

    </form>
    
    <a href="/borang-cuti" class='btn btn-primary'>Mohon Cuti</a>
    <table class="table table-striped">
        <tr>
            <th>Bil</th>
            <th>Jenis Cuti</th>
            <th>Tarikh Mula</th>
            <th>Tarikh Tamat</th>
            <th>Status</th>
            <th>Tindakan</th>
        </tr>
        @php
        $i = 1;
        @endphp
        @foreach($leaves as $m)
        <tr>
            <td>{{$i++}}</td>
            <td>{{$m->refs->descr}}</td>
            <td>{{date('d-m-Y',strtotime($m->start_dt))}}</td>
            <td>{{$m->end_dt}}</td>
            <td>{{$m->lvsts->descr}}</td>
            <td>                 
                <form id="delete_{{$m->id}}" action="hapus-cuti/{{$m->id}}" method='POST'>
                    @csrf
                    @method('DELETE')
                    <a href="/borang-cuti/{{$m->id}}" class='btn btn-secondary'>Kemaskini</a>
                    <a href="#" class='btn btn-danger' onclick="deleteRecord({{$m->id}})">Hapus</a>
                </form>
        </td>
        </tr>
        @endforeach
    </table>
    {{$leaves->appends(['leave_type'=>$leave_type])->links() }}
    @endsection

 