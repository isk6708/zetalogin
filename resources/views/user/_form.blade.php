@extends('layouts.zeta_login')
@section('title', 'Borang Pengguna')
@section('content')
    <h1>Borang Pengguna</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($user->id))
    <form action="{{route('pengguna.update',$user->id)}}" method='POST' enctype="multipart/form-data">
        @method('PATCH')
    @else
    <!-- <form action="{{route('pengguna.store')}}" method='POST' enctype="multipart/form-data"> -->
    <form action="/user" method='POST' enctype="multipart/form-data">
    @endif
        @csrf
        <img src="{{$avatar}}" alt="avatar" 
    class="rounded-circle" height="48" width="48">
        <div class="col-sm-4">
            <div class="mb-3">
                <label for="name" class="form-label">Nama<span class="mandatory">*</span></label>
                <input type="text" name="name" id="name" value="{{old('name', $user)}}" class="form-control">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email<span class="mandatory">*</span></label>
                <input type="text" name="email" id="email" value="{{old('email', $user)}}" class="form-control">
            </div>
            <div class="mb-3">
                <label for="end_dt" class="form-label">Katalaluan<span class="mandatory">*</span></label>
                <input type="password" name="katalaluan" id="katalaluan" value="{{old('password', $user)}}" class="form-control">
            </div>
            <div class="mb-3">
                <label for="confirmed" class="form-label">Pengesahan Katalaluan<span class="mandatory">*</span></label>
                <input type="password" name="katalaluan_confirmation" id="katalaluan_confirmation" value="{{old('password', $user)}}" class="form-control">
            </div>

            <div class="mb-3">
                <label for="attachment" class="form-label">Gambar Profil<span class="mandatory">*</span></label>
                <input type="file" name="attachment" id="attachment" class="form-control">
            </div>
            <div class="mb-3">
                <input type="submit" value="{{empty($user->id)?'Simpan':'Kemaskini'}}" class='btn btn-primary'>
                @if(!empty($user->id))
                <a href="{{route('pengguna.edit',$user->id)}}" class='btn btn-secondary'>Set Semula</a>
                @else
                <a href="{{route('pengguna.create')}}" class='btn btn-secondary'>Set Semula</a>
                @endif
                <a href="{{route('pengguna.index')}}" class='btn btn-danger'>Kembali</a>
            </div>
        </div>

    </form>
    @endsection