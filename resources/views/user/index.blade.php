@extends('layouts.zeta_login')
@section('title', 'Senarai Pengguna')
@section('content')
<h2>Senarai Pengguna</h2>
<a href="{{route('pengguna.create')}}" class='btn btn-primary'>Tambah Pengguna</a>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama</th>
        <th>Tindakan</th>
    </tr>

@php 
$i = 1;
@endphp
@foreach($users as $m)
<tr>
    <td>{{$i++}}</td>
    <td>{{$m->name}}</td>
    <td>
    <form id="delete_{{$m->id}}" action="{{route('pengguna.destroy',$m->id)}}" method='POST'>
        @csrf
        @method('DELETE')
        <a href="{{route('pengguna.edit',$m->id)}}" class='btn btn-secondary'>Kemaskini</a>
        <a href="#" class='btn btn-danger' onclick="deleteRecord({{$m->id}})">Hapus</a>
    </form>
    </td>
</tr>
@endforeach
</table>
@endsection