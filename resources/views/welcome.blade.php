@extends('layouts.zeta_login')
@section('title', 'Selamat Datang')
@section('content')
<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css' href='https://www.zetasb.com.my/wp-includes/css/dist/block-library/style.min.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='classic-theme-styles-css' href='https://www.zetasb.com.my/wp-includes/css/classic-themes.min.css?ver=1' media='all' />
<style id='global-styles-inline-css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;--wp--preset--spacing--20: 0.44rem;--wp--preset--spacing--30: 0.67rem;--wp--preset--spacing--40: 1rem;--wp--preset--spacing--50: 1.5rem;--wp--preset--spacing--60: 2.25rem;--wp--preset--spacing--70: 3.38rem;--wp--preset--spacing--80: 5.06rem;}:where(.is-layout-flex){gap: 0.5em;}body .is-layout-flow > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-flow > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-flow > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-constrained > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-constrained > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > :where(:not(.alignleft):not(.alignright):not(.alignfull)){max-width: var(--wp--style--global--content-size);margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignwide{max-width: var(--wp--style--global--wide-size);}body .is-layout-flex{display: flex;}body .is-layout-flex{flex-wrap: wrap;align-items: center;}body .is-layout-flex > *{margin: 0;}:where(.wp-block-columns.is-layout-flex){gap: 2em;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
.wp-block-navigation a:where(:not(.wp-element-button)){color: inherit;}
:where(.wp-block-columns.is-layout-flex){gap: 2em;}
.wp-block-pullquote{font-size: 1.5em;line-height: 1.6;}
</style>
<link rel='stylesheet' id='contact-form-7-css' href='https://www.zetasb.com.my/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.5.1' media='all' />
<link rel='stylesheet' id='bootstrap-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/bootstrap.min.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='font-awesome-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/font-awesome.min.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='tanda-icons-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/themify-icons.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='tanda-flaticons-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/flaticon-set.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='magnific-popup-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/magnific-popup.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='owl-carousel-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/owl.carousel.min.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='owl-carousal-default-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/owl.theme.default.min.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='animate-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/animate.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='bootsnav-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/bootsnav.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='tanda-style-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/style.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='tanda-responsive-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/responsive.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='tanda-fonts-css' href='//fonts.googleapis.com/css?family=Inter%3A200%2C300%2C400%2C600%2C700%2C800%26subset%3Dlatin%2Clatin-ext&#038;ver=1.0.0' media='all' />
<link rel='stylesheet' id='tanda-color-css' href='https://www.zetasb.com.my/wp-content/themes/tanda/css/theme-color/color-1.css?ver=6.1.1' media='all' />
<link rel='stylesheet' id='js_composer_front-css' href='https://www.zetasb.com.my/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.7.0' media='all' />
<script src='https://www.zetasb.com.my/wp-includes/js/jquery/jquery.min.js?ver=3.6.1' id='jquery-core-js'></script>
<script src='https://www.zetasb.com.my/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>

<!-- Google Analytics snippet added by Site Kit -->
<script src='https://www.googletagmanager.com/gtag/js?id=UA-222695519-1' id='google_gtagjs-js' async></script>
<script id='google_gtagjs-js-after'>
window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('set', 'linker', {"domains":["www.zetasb.com.my"]} );
gtag("js", new Date());
gtag("set", "developer_id.dZTNiMT", true);
gtag("config", "UA-222695519-1", {"anonymize_ip":true});
gtag("config", "G-PFZEZ5NZTN");
</script>

<!-- End Google Analytics snippet added by Site Kit -->
<link rel="https://api.w.org/" href="https://www.zetasb.com.my/wp-json/" /><link rel="alternate" type="application/json" href="https://www.zetasb.com.my/wp-json/wp/v2/pages/258" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.zetasb.com.my/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.zetasb.com.my/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 6.1.1" />
<link rel='shortlink' href='https://www.zetasb.com.my/' />
<link rel="alternate" type="application/json+oembed" href="https://www.zetasb.com.my/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.zetasb.com.my%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.zetasb.com.my/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.zetasb.com.my%2F&#038;format=xml" />
<meta name="framework" content="Redux 4.3.1" /><meta name="generator" content="Site Kit by Google 1.94.0" />
<!-- Google AdSense snippet added by Site Kit -->
<meta name="google-adsense-platform-account" content="ca-host-pub-2644536267352236">
<meta name="google-adsense-platform-domain" content="sitekit.withgoogle.com">
<!-- End Google AdSense snippet added by Site Kit -->
<style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>
<body class="home page-template page-template-page-templates page-template-home4 page-template-page-templateshome4-php page page-id-258 wpb-js-composer js-comp-ver-6.7.0 vc_responsive">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>    
    <!-- Preloader Start -->
    <div id="site-preloader" class="site-preloader">
        <div class="loader-wrap">
            <div class="ring">
                <span></span>
            </div>
        </div>
    </div>
    <!-- Preloader Ends -->
    <!-- Header 
    ============================================= -->
    <header id="home">

        <!-- Start Navigation -->
        <nav class="navbar navbar-default navbar-fixed dark no-background bootsnav">

            <!-- Start Top Search -->
            <div class="container-full">
                <div class="row">
                    <div class="top-search">
                        <div class="input-group">
                             <form role="search" method="get" action="https://www.zetasb.com.my/">
                    <input type='search' name="s" placeholder="Search Here..." class="form-control" id="search-box" value="" >
                                <button type="submit">
                                    <i class="fas fa-search"></i>
                                </button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-full">

                <!-- Start Atribute Navigation -->
                <div class="attr-nav inc-border">
                    <ul>
                        <li class="search"><a href="#"><i class="ti-search"></i></a></li>
                        <li class="side-menu"><a href="#"><i class="ti-info-alt"></i></a></li>
                    </ul>
                </div>        
                <!-- End Atribute Navigation -->

                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="https://www.zetasb.com.my/">
                        <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/zeta-logo-1.png" class="logo" alt="Logo">
                    </a>
                </div>
                <!-- End Header Navigation -->

                 <div id="navbar-menu" class="collapse navbar-collapse"><ul data-in="#" data-out="#" class="nav navbar-nav navbar-center" id="menu-main-menu-4"><li id="menu-item-641" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-258 current_page_item menu-item-641 active"><a   title="Home" href="https://www.zetasb.com.my/">Home</a></li>
<li id="menu-item-659" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-659 dropdown"><a   title="Company" href="#" class="dropdown-toggle" data-toggle="dropdown">Company</a>
<ul class=" dropdown-menu" >
	<li id="menu-item-660" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-660"><a   title="About Company" href="https://www.zetasb.com.my/about-us/">About Company</a></li>
	<li id="menu-item-661" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-661"><a   title="Why Choose Us" href="https://www.zetasb.com.my/why-choose-us/">Why Choose Us</a></li>
	<li id="menu-item-1161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1161"><a   title="Our Clients" href="https://www.zetasb.com.my/client/">Our Clients</a></li>
	<li id="menu-item-663" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-663"><a   title="Meet Our Team" href="https://www.zetasb.com.my/?page_id=126">Meet Our Team</a></li>
</ul>
</li>
<li id="menu-item-1062" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1062 dropdown"><a   title="Services" href="https://www.zetasb.com.my/services/" class="dropdown-toggle" data-toggle="dropdown">Services</a>
<ul class=" dropdown-menu" >
	<li id="menu-item-1207" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1207"><a   title="Web Application Development" href="https://www.zetasb.com.my/services-web/">Web Application Development</a></li>
	<li id="menu-item-1208" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1208"><a   title="Networking" href="https://www.zetasb.com.my/services-networking/">Networking</a></li>
	<li id="menu-item-1209" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1209"><a   title="Supply of ICT Hardware" href="https://www.zetasb.com.my/services-hardware/">Supply of ICT Hardware</a></li>
	<li id="menu-item-1210" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1210"><a   title="Consultancy and Training." href="https://www.zetasb.com.my/services-training/">Consultancy and Training.</a></li>
</ul>
</li>
<li id="menu-item-1058" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1058"><a   title="Projects" href="https://www.zetasb.com.my/our-project/">Projects</a></li>
<li id="menu-item-1267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1267"><a   title="Join Us" href="https://www.zetasb.com.my/join/">Join Us</a></li>
<li id="menu-item-704" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-704"><a   title="Contact Us" href="https://www.zetasb.com.my/contact-us/">Contact Us</a></li>
</ul></div>            </div>

             <!-- Start Side Menu -->
            <div class="side">
                <a href="#" class="close-side"><i class="ti-close"></i></a>
                <div class="widget">
                    <h4 class="title">Contact Info</h4>
                    <ul class="contact">
                        <li>
                            <div class="icon">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="info">
                                <span>Phone</span> +603 5510 1670                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-envelope-open"></i>
                            </div>
                            <div class="info">
                                <span>Email</span> sales@zetasb.com.my                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-clock"></i>
                            </div>
                            <div class="info">
                                <span>OFFICE HOURS</span> Office Hours: 9:00 AM – 5:00 PM                            </div>
                        </li>
                    </ul>
                </div>
                <div class="widget">
                    <h4 class="title">Additional Links</h4>
                    <div class="menu-additional-links-container"><ul id="menu-additional-links" class="menu"><li id="menu-item-306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-306"><a href="https://www.zetasb.com.my/about-us/">About</a></li>
<li id="menu-item-305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-305"><a href="https://www.zetasb.com.my/client/">Our Clients</a></li>
<li id="menu-item-304" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-304"><a href="https://www.zetasb.com.my/contact-us/">Contact</a></li>
</ul></div>                </div>
                <div class="widget social">
                    <h4 class="title"></h4>
                    <ul class="link">
                        <li class="facebook"><a href=""><i class=""></i></a></li>
                        <li class="twitter"><a href=""><i class=""></i></a></li>
                        <li class="pinterest"><a href=""><i class=""></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- End Side Menu -->

        </nav>
        <!-- End Navigation -->

    </header>
    <!-- End Header -->
 <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- Start Banner 
    ============================================= -->
    <div class="banner-area home2-banner-fix shape center-item text-color">
        <div class="item">
            <div class="container">
                <div class="row align-center">
                                    
                    <div class="col-lg-6">
                        <div class="content">
                            <h2 class="wow fadeInDown">We build solutions <strong>that work</strong></h2>
                            <p class="wow fadeInLeft">
                                Zeta is providing end-to-end integrated solutions, for a wide range of Information &amp; Communication Technology (ICT) products and services carrying international standards and quality.
                            </p>
                            <div class="bottom">
                                <a class="btn btn-theme effect btn-md wow fadeInUp" href="/contact-us">Start A Project</a>
                                <div class="call-us wow fadeInLeft">
                                    <div class="icon">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                    <div class="info">
                                        <h5>+603 5510 1670</h5>
                                        <span>Call for any question</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 thumb">
                        <img class="wow fadeInUp" src="https://www.zetasb.com.my/wp-content/uploads/2021/10/home.png" alt="Thumb">
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Banner --></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- Star Featured Services Area
    ============================================= -->
    <div class="featured-services-area text-center default-padding-bottom bottom-less">
        <!-- Fixed Shape  -->
        <div class="fixed-shape-left-top">
            <img src="https://www.zetasb.com.my/wp-content/uploads/2021/01/7.png" alt="Shape">
        </div>
        <!-- End Fixed Shape  -->
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>Services</h4>
                        <h2>What we do</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row"><!-- Single item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="info">
                                <h4>Web Application Development</h4>
                                <i class="flaticon-cogwheel"></i>
                                <p>
                                    As major activities of the company, we do full range web application development and system integration.
                                </p>
                                <a class="btn-standard" href="/services-web/">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single item --><!-- Single item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="info">
                                <h4>Networking</h4>
                                <i class="flaticon-cloud-storage"></i>
                                <p>
                                    We are able to assist customer organizations in planning, designing, installing, and commissioning their network infrastructure
                                </p>
                                <a class="btn-standard" href="/services-networking/">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single item --><!-- Single item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="info">
                                <h4>Supply of ICT Hardware</h4>
                                <i class="flaticon-maintenance"></i>
                                <p>
                                    With strong support from major brand in ICT hardware and our in-house expertise, we are capable to deliver full scale of ICT hardware peripherals for our client.
                                </p>
                                <a class="btn-standard" href="/services-hardware/">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single item --><!-- Single item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="info">
                                <h4>Consultancy and Training</h4>
                                <i class="flaticon-technical-support"></i>
                                <p>
                                    Complement our other offerings by providing comprehensive training supports for our products.
                                </p>
                                <a class="btn-standard" href="/services-training/">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single item --></div>
            </div>
        </div>
    </div>
    <!-- End Services Area --></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- Start Case Studies Area
    ============================================= -->
    <div class="case-studies-area half-bg default-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>PREVIOUS PROJECTS</h4>
                        <h2>Our Work Showcase</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fill">
            <div class="case-carousel owl-carousel owl-theme"><div class="item">
    <div class="thumb">
        <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg" alt="Thumb">
    </div>
    <div class="info">
        <div class="info-items">
            <div class="left-info">
                <h4>
                    <a href="/cyber-security-casestudies/">Turcomp Engineering Services Sdn Bhd.</a>
                </h4>
                <ul>
                    <li>Software Development</li>
                </ul>
            </div>
            <div class="right-info">
                <a class="item popup-gallery" href="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div><div class="item">
    <div class="thumb">
        <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg" alt="Thumb">
    </div>
    <div class="info">
        <div class="info-items">
            <div class="left-info">
                <h4>
                    <a href="/cyber-security-casestudies/">Projek Lebuhraya Utara Selatan(PLUS) Sdn.Bhd</a>
                </h4>
                <ul>
                    <li>Software Development</li>
                </ul>
            </div>
            <div class="right-info">
                <a class="item popup-gallery" href="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div><div class="item">
    <div class="thumb">
        <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg" alt="Thumb">
    </div>
    <div class="info">
        <div class="info-items">
            <div class="left-info">
                <h4>
                    <a href="/cyber-security-casestudies/">Bahagian Perkhidmatan Farmasi,Kementerian Kesihatan Malaysia.</a>
                </h4>
                <ul>
                    <li>Maintenance</li>
                </ul>
            </div>
            <div class="right-info">
                <a class="item popup-gallery" href="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div><div class="item">
    <div class="thumb">
        <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg" alt="Thumb">
    </div>
    <div class="info">
        <div class="info-items">
            <div class="left-info">
                <h4>
                    <a href="/cyber-security-casestudies/">Bahagian Teknologi Pendidikan,Kementerian Pendidikan Malaysia.</a>
                </h4>
                <ul>
                    <li>Software Development</li>
                </ul>
            </div>
            <div class="right-info">
                <a class="item popup-gallery" href="https://www.zetasb.com.my/wp-content/uploads/2021/10/banner-redimensionat.jpg"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div></div>
        </div>
    </div>
    <!-- End Case Studies Area --><div class="vc_btn3-container  bg-gray pt-3 pb-3 vc_btn3-center" ><a style="background-color:#086ad8; color:#ffffff;" class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-left" href="https://zetasb.hitsolution.biz.my/our-project/" title="Our Projects"><i class="vc_btn3-icon fas fa-angle-right"></i> View More</a></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- Start Works About 
    ============================================= -->
    <div class="works-about-area overflow-hidden half-bg">
        <div class="container">
            <div class="works-about-items default-padding">
                <div class="row align-center">
                    <div class="col-lg-6 info">
                        <h5>Works About</h5>
                        <h2>Trusted by 50+<br>Happy Customers</h2>
                        <p>
                             We have a resource pool of top notch ICT specialists and individuals to provide smart solutions to our clients and partners. Our team of competent partners, associates, and staff with adequate qualification and credentials, makes up to more than 20 years of services experience. 
                        </p>
                        <ul>
                            <li>
                                <h5>100% Client Satisfaction</h5>
                            </li>
                            <li>
                                <h5>World Class Worker</h5>
                            </li>
                        </ul><a href="/projects" class="btn btn-theme effect btn-sm">Completed Projects</a></div>
                    <div class="col-lg-6">
                        <div class="thumb">
                            <img src="https://www.zetasb.com.my/wp-content/uploads/2021/01/3.jpg" alt="Thumb">
                            <div class="fun-fact">
                                <div class="timer" data-to="100" data-speed="5000"></div>
                                <span class="medium">Completed Projects</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Works About Area --></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- Start Clients 
    ============================================= -->
    <div class="clients-area bg-gray default-padding bottom-less">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="clients-carousel owl-carousel owl-theme"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/paab-png28.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/1200px-Seal_of_Kuala_Lumpur.svg.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/Dbplogo.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/Prolintas.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/upsi.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/turcomp.png" alt="Client"><img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/plus.png" alt="Client"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Clients Area --></div></div></div></div>

 <!-- Start Footer 
    ============================================= -->
    <footer class="bg-dark text-light">
        <!-- Fixed Shape -->
        <div class="fixed-shape">
            <img src="https://www.zetasb.com.my/wp-content/themes/tanda/img/map.svg" alt="Shape">
        </div>
        <!-- Fixed Shape -->
        <div class="container">
            <div class="f-items default-padding">
                <div class="row">
                    <div class="col-lg-4 col-md-6 item">
                        <div class="f-item about">
                            <img src="https://www.zetasb.com.my/wp-content/uploads/2021/10/zeta-logo-1.png" alt="Logo">
                            <p>
                                Zeta Integrated Solution Sdn. Bhd. (Zeta) is providing end-to-end integrated solutions, for a wide range of Information &amp; Communication Technology (ICT) products and services carrying international standards and quality.                            </p>
                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-6 item">
                        <div class="f-item link">
                            <h4 class="widget-title">Company</h4>
                            <div class="menu-company-container"><ul id="menu-company" class="menu"><li id="menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-312"><a href="https://www.zetasb.com.my/about-us/">About Us</a></li>
<li id="menu-item-314" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-314"><a href="https://www.zetasb.com.my/?page_id=126">Meet Our Team</a></li>
<li id="menu-item-313" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-313"><a href="https://www.zetasb.com.my/client/">Our Clients</a></li>
<li id="menu-item-315" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-315"><a href="https://www.zetasb.com.my/why-choose-us/">Why Choose Us</a></li>
</ul></div>                        </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-6 item">
                        <div class="f-item link">
                            <h4 class="widget-title">Solutions</h4>
                            <div class="menu-solutions-container"><ul id="menu-solutions" class="menu"><li id="menu-item-1211" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1211"><a href="https://www.zetasb.com.my/services-training/">Consultancy and Training.</a></li>
<li id="menu-item-1212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1212"><a href="https://www.zetasb.com.my/services-hardware/">Supply of ICT Hardware</a></li>
<li id="menu-item-1213" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1213"><a href="https://www.zetasb.com.my/services-networking/">Networking</a></li>
<li id="menu-item-1214" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1214"><a href="https://www.zetasb.com.my/services-web/">Web Application Development</a></li>
</ul></div>                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 item">
                    <div class="f-item">
                        <h4 class="widget-title">Contact Info</h4>
                        <div class="address">
                            <ul>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-pin"></i>
                                    </div>
                                    <div class="info">
                                        Block 4, Suite 229, No. 7 Persiaran Sukan, Laman Seri Business Park,
Seksyen 13, 40100 Shah Alam, Selangor Darul Ehsan.                                    </div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-email-1"></i>
                                    </div>
                                    <div class="info">
                                        <a href="mailto:http://sales@zetasb.com.my">sales@zetasb.com.my</a> <br> <a href="mailto:"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <i class="flaticon-telephone"></i>
                                    </div>
                                    <div class="info">
                                        +603 5510 1670 <br>                                     </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>&copy; Copyright 2021. Zeta Integrated Solution Sdn. Bhd. </p>
                    </div>
                    <div class="col-md-6 text-right link">
                        <div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-1229" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-258 current_page_item menu-item-1229"><a href="https://www.zetasb.com.my/" aria-current="page">Home</a></li>
<li id="menu-item-309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309"><a href="https://www.zetasb.com.my/about-us/">About</a></li>
<li id="menu-item-1230" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1230"><a href="https://www.zetasb.com.my/services/">Services</a></li>
<li id="menu-item-311" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311"><a href="https://www.zetasb.com.my/contact-us/">Contact</a></li>
</ul></div>                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </footer>
    <!-- End Footer -->
<link rel='stylesheet' id='vc_font_awesome_5_shims-css' href='https://www.zetasb.com.my/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/v4-shims.min.css?ver=6.7.0' media='all' />
<link rel='stylesheet' id='vc_font_awesome_5-css' href='https://www.zetasb.com.my/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/all.min.css?ver=6.7.0' media='all' />
<script src='https://www.zetasb.com.my/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9' id='regenerator-runtime-js'></script>
<script src='https://www.zetasb.com.my/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0' id='wp-polyfill-js'></script>
<script id='contact-form-7-js-extra'>
var wpcf7 = {"api":{"root":"https:\/\/www.zetasb.com.my\/wp-json\/","namespace":"contact-form-7\/v1"}};
</script>
<script src='https://www.zetasb.com.my/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.5.1' id='contact-form-7-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/popper.min.js?ver=6.1.1' id='popper-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/bootstrap.min.js?ver=6.1.1' id='bootstrap-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/equal-height.min.js?ver=6.1.1' id='equal-height-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/jquery.appear.js?ver=6.1.1' id='jquery-appear-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/jquery.easing.min.js?ver=6.1.1' id='jquery-easing-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/jquery.magnific-popup.min.js?ver=6.1.1' id='magnific-popup-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/modernizr.custom.13711.js?ver=6.1.1' id='modernizr-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/owl.carousel.min.js?ver=6.1.1' id='owl-carousel-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/wow.min.js?ver=6.1.1' id='wow-min-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/progress-bar.min.js?ver=6.1.1' id='progress-bar-js'></script>
<script src='https://www.zetasb.com.my/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=6.7.0' id='isotope-js'></script>
<script src='https://www.zetasb.com.my/wp-includes/js/imagesloaded.min.js?ver=4.1.4' id='imagesloaded-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/count-to.js?ver=6.1.1' id='count-to-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/YTPlayer.min.js?ver=6.1.1' id='ytplayer-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/jquery.nice-select.min.js?ver=6.1.1' id='jquery-nice-select-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/bootsnav.js?ver=6.1.1' id='bootsnav-js'></script>
<script src='https://www.zetasb.com.my/wp-content/themes/tanda/js/main.js?ver=6.1.1' id='tanda-main-js'></script>
<script src='https://www.google.com/recaptcha/api.js?render=6Lfbm8gcAAAAAKP95tWLf_s_5hZwL1AXZClbFxqm&#038;ver=3.0' id='google-recaptcha-js'></script>
<script id='wpcf7-recaptcha-js-extra'>
var wpcf7_recaptcha = {"sitekey":"6Lfbm8gcAAAAAKP95tWLf_s_5hZwL1AXZClbFxqm","actions":{"homepage":"homepage","contactform":"contactform"}};
</script>
<script src='https://www.zetasb.com.my/wp-content/plugins/contact-form-7/modules/recaptcha/index.js?ver=5.5.1' id='wpcf7-recaptcha-js'></script>
<script src='https://www.zetasb.com.my/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.7.0' id='wpb_composer_front_js-js'></script>
			<script src="https://www.google.com/recaptcha/api.js?render=6LfqW1IdAAAAAJmqnNkvhSYWA7iQKQ_Ipn6HMgSV"></script>
			<script type="text/javascript">
				( function( grecaptcha ) {

					var anr_onloadCallback = function() {
						grecaptcha.execute(
							'6LfqW1IdAAAAAJmqnNkvhSYWA7iQKQ_Ipn6HMgSV',
							{ action: 'advanced_nocaptcha_recaptcha' }
						).then( function( token ) {
							for ( var i = 0; i < document.forms.length; i++ ) {
								var form = document.forms[i];
								var captcha = form.querySelector( 'input[name="g-recaptcha-response"]' );
								if ( null === captcha )
									continue;

								captcha.value = token;
							}
						});
					};

					grecaptcha.ready( anr_onloadCallback );

					document.addEventListener( 'wpcf7submit', anr_onloadCallback, false );
					if ( typeof jQuery !== 'undefined' ) {
						//Woocommerce
						jQuery( document.body ).on( 'checkout_error', anr_onloadCallback );
					}
					//token is valid for 2 minutes, So get new token every after 1 minutes 50 seconds
					setInterval(anr_onloadCallback, 110000);

				} )( grecaptcha );
			</script>
			

@endsection