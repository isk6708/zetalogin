<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\ReferenceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware('auth')->group(function () {
    Route::get('/senarai-cuti', [LeaveController::class,'SenaraiCuti']);
    Route::get('/borang-cuti/{id?}', [LeaveController::class,'MohonCuti'])->name('borang.cuti');//create&edit
    Route::post('/simpan-cuti/{id?}', [LeaveController::class,'SimpanCuti'])->name('simpan.cuti');//store&update


    Route::get('/cipta-cuti', [LeaveController::class,'BorangCuti'])->name('mohon.cuti');//create
    Route::post('/cipta-cuti', [LeaveController::class,'CiptaCuti'])->name('cipta.cuti');//store
    Route::get('/kemaskini-cuti/{id}', [LeaveController::class,'UbahCuti'])->name('ubah.cuti');//edit
    Route::post('/kemaskini-cuti/{id}', [LeaveController::class,'KemaskiniCuti'])->name('kemaskini.cuti');//update

    Route::post('/batal-cuti', [LeaveController::class,'BatalCuti']);
    Route::post('/status-cuti/{jenis_cuti}/{status?}', [LeaveController::class,'StatusCuti']);
    Route::delete('/hapus-cuti/{id}', [LeaveController::class,'HapusCuti']);
    
    // Route::resource('references', ReferenceController::class);

    Route::prefix('reference')->group(function () {
        
            Route::get('/index', [ReferenceController::class,'index'])->name('reference.index');
            Route::get('/create', [ReferenceController::class,'create'])->name('reference.create');
            Route::post('/store', [ReferenceController::class,'store'])->name('reference.store');
            Route::get('/edit/{id}', [ReferenceController::class,'edit'])->name('reference.edit');
            Route::patch('/update/{id}', [ReferenceController::class,'update'])->name('reference.update');
            Route::delete('/destroy/{id}', [ReferenceController::class,'destroy'])->name('reference.destroy');
        
    });
    
    // Route::get('/reference/index', [ReferenceController::class,'index'])->name('reference.index');
    // Route::get('/reference/create', [ReferenceController::class,'create'])->name('reference.create');
    // Route::post('/reference/store', [ReferenceController::class,'store'])->name('reference.store');
    // Route::get('/reference/edit/{id}', [ReferenceController::class,'edit'])->name('reference.edit');
    // Route::patch('/reference/update/{id}', [ReferenceController::class,'update'])->name('reference.update');
    // Route::delete('/reference/destroy/{id}', [ReferenceController::class,'destroy'])->name('reference.destroy');


    Route::resource('pengguna', UserController::class);


});





require __DIR__.'/auth.php';
